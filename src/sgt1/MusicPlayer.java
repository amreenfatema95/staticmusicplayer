package sgt1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class MusicPlayer extends JFrame
{
	public MusicPlayer() throws IOException
	{
		musicPlayerGUI();
	}
	
	public void musicPlayerGUI() throws IOException
	{
		setSize(300,400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Track 0");
		setLayout(new BorderLayout());
		JPanel topPanel = new JPanel(new GridLayout(1,3));
		add(topPanel,BorderLayout.NORTH);
		ImageIcon replayImage = new ImageIcon("replay.png");
		JLabel replay = new JLabel(replayImage,JLabel.LEFT);
		topPanel.add(replay);
		topPanel.setBackground(Color.BLACK);
		String Etime = "<html><tag><h3 font color =#FFFFFF><b> Elapsed Time:  04.37 </b></h3></font></tag></html>";
		JLabel time = new JLabel(Etime);
		topPanel.add(time);
		ImageIcon shuffleImage = new ImageIcon("Shuffle.png");
		JLabel shuffle = new JLabel(shuffleImage,JLabel.RIGHT);
		topPanel.add(shuffle);
		
		JPanel center = new JPanel(new BorderLayout());
		add(center,BorderLayout.CENTER);
		BufferedImage myPicture = ImageIO.read(new File("controls.png"));
		JLabel picLabel = new JLabel(new ImageIcon(myPicture));
		center.add(picLabel);
		
		JPanel bottom = new JPanel(new GridLayout(9,3));
		add(bottom,BorderLayout.SOUTH);
		bottom.setBackground(Color.BLACK);
		
		Border border = BorderFactory.createLineBorder(Color.WHITE, 1);
		for(int i=0;i<9;i++)
		{
			JLabel number = new JLabel("<html><tag><h3 font color= #FFFFFF>0"+(i+1)+" </font></h3></tag></html>",JLabel.CENTER);
			number.setBorder(border);
			bottom.add(number);
			JLabel TrackTitle = new JLabel("<html><tag><h3 font color= #FFFFFF>Track "+i+"</font></h3></tag></html>",JLabel.CENTER);
			TrackTitle.setBorder(border);
			bottom.add(TrackTitle);
			JLabel TrackTime = new JLabel("<html><tag><h3 font color= #FFFFFF> 00:00 </font></h3></tag></html> ",JLabel.CENTER);
			TrackTime.setBorder(border);
			bottom.add(TrackTime);
		}
		pack();
	}
}
